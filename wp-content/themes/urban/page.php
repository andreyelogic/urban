<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package urban
 */
global $post;
get_header();
?>

    <div class="col-xs-12 no_padding_for_mobile">
        <?php
        get_template_part( 'template-parts/page',$post->post_name ); ?>


    </div>


<?php

get_footer();
