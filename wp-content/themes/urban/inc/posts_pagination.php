<?php
$page = (get_query_var('page')) ? get_query_var('page') : 1;
$args = ['paged' => $page];
if (isset($cat_name)) {
    $args['category_name'] = $cat_name;
}
if (isset($per_page)) {
    $args['posts_per_page'] = $per_page;
}
if (isset($category_in))
{
    $args['category__in'] = $category_in;
}
$query = new WP_Query($args);
?>
<div class="blog">

    <?php
    if ($title) {
        include get_template_directory() . '/template-parts/title-of-page.php';
    }
    ?>

    <div class="col-xs-12">
        <?php
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post(); ?>
                <a href="<?= get_permalink(); ?>" class="one_new_read_more_btn">
                    <div class="col-md-<?= $columns; ?>">

                        <?php
                        if (has_post_video(get_the_ID())) { ?>
                            <div class="blog_image_post">
                                <?php
                                echo do_shortcode('[featured-video-plus]'); ?>
                            </div>
                            <?php
                        } else { ?>
                            <div class="blog_image_post"
                                 style="background-image: url('<?= get_the_post_thumbnail_url() ?>')"></div>
                        <?php }
                        ?>



                        <div class="blog_title_post">
                            <?php the_title(); ?>
                        </div>
                    </div>
                </a>
                <?php

            }
        } else {
            get_template_part('template-parts/content', 'none');
        } ?>
    </div>
    <?php

    if ($query->max_num_pages > 1) { ?>
        <div class="posts_pagination col-xs-12">
            <?php
            $i = 1;
            while ($i < $query->max_num_pages + 1) { ?>
                <a href="?page=<?= $i; ?>" class="pag <?php if ($page == $i) {
                    echo 'active';
                } ?> "><?= $i; ?></a>
                <?php
                $i++;
            }
            ?>
        </div>
    <?php }
    ?>
</div>