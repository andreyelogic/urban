<?php
add_action('admin_menu', function(){
    add_menu_page( 'Додаткові налаштування', 'Додаткові налаштування', 'manage_options', 'site-options-advanced', 'my_custom_settings', '', 0 );
//    wp_enqueue_style( 'urban-style-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'urban-admin-style', get_template_directory_uri() . '/inc/admin/admin.css');
    wp_enqueue_script( 'urban-admin-script', get_template_directory_uri() . '/inc/admin/admin.js', '', '', true);
} );
function my_custom_settings()
{
    include 'advanced-options-view.php';
}