<?php
if (isset($_POST['tel_num_one']) OR isset($_POST['tel_num_two']) OR isset($_POST['adress_text']) OR isset($_POST['mail_for_contact'])) {
    if (!empty($_POST['tel_num_one'])) {
        update_option('tel_num_one', $_POST['tel_num_one']);
    }
    if (!empty($_POST['tel_num_two'])) {
        update_option('tel_num_two', $_POST['tel_num_two']);
    }
    if (!empty($_POST['mail_for_contact'])) {
        update_option('mail_for_contact', $_POST['mail_for_contact']);
    }

    if (!empty($_POST['adress_country'])) {
        update_option('adress_country', $_POST['adress_country']);
    }
    if (!empty($_POST['adress_city'])) {
        update_option('adress_city', $_POST['adress_city']);
    }
    if (!empty($_POST['adress_street'])) {
        update_option('adress_street', $_POST['adress_street']);
    } ?>
    <div class="margin_right_top">
        <div class="alert alert-success">
            Контактні налаштування збережені
        </div>
    </div>
    <?php
}

if (isset($_POST['coordinats_lat']) OR isset($_POST['coordinats_lng'])) {
    if (!empty($_POST['coordinats_lat'])) {
        update_option('coordinats_lat', $_POST['coordinats_lat']);
    }
    if (!empty($_POST['coordinats_lng'])) {
        update_option('coordinats_lng', $_POST['coordinats_lng']);
    } ?>
    <div class="margin_right_top">
        <div class="alert alert-success">
            Налаштування мапи збережені
        </div>
    </div>
    <?php
}


if (isset($_POST['social_linkedin']) OR isset($_POST['social_facebook']) OR isset($_POST['social_twitter'])) {
    if (!empty($_POST['social_linkedin'])) {
        update_option('social_linkedin', $_POST['social_linkedin']);
    }
    if (!empty($_POST['social_facebook'])) {
        update_option('social_facebook', $_POST['social_facebook']);
    }
    if (!empty($_POST['social_twitter'])) {
        update_option('social_twitter', $_POST['social_twitter']);
    }
    ?>
    <div class="margin_right_top">
        <div class="alert alert-success">
            Налаштування соціальних мереж збережені
        </div>
    </div>
    <?php
}
?>

<div>
    <div class="col-xs-12 advanced_settings_wrapper">
        <h2 class="advanced_settings_title">Додаткові налаштування</h2>

        <div class="one_settings_block">
            <div class="settings_block_title">
                Налаштування контанктних даних
            </div>
            <div class="settings_block small">
                <form method="post" class="contact-form">
                    <div class="elem">
                        <label for="">Контактний номер телефону</label>
                        <input type="text" name="tel_num_one" value="<?= get_option('tel_num_one'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Контактний номер телефону ( додатковий )</label>
                        <input type="text" name="tel_num_two" value="<?= get_option('tel_num_two'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Країна</label>
                        <input type="text" name="adress_country" value="<?= get_option('adress_country'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Місто</label>
                        <input type="text" name="adress_city" value="<?= get_option('adress_city'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Вулиця</label>
                        <input type="text" name="adress_street" value="<?= get_option('adress_street'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Ваша пошта</label>
                        <input type="text" name="mail_for_contact" value="<?= get_option('mail_for_contact'); ?>">
                    </div>
                    <div class="elem for_save_btn_padding">
                        <input type="submit" value="Зберегти" class="btn btn-default width_auto">
                    </div>
                </form>
            </div>
        </div>

        <div class="one_settings_block">
            <div class="settings_block_title">
                Налаштування мапи
            </div>
            <div class="settings_block smaller">
                <form method="post" class="contact-form">
                    <div class="elem">
                        <label for="">Координати широти</label>
                        <input type="text" name="coordinats_lat" value="<?= get_option('coordinats_lat'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Координати довготи</label>
                        <input type="text" name="coordinats_lng" value="<?= get_option('coordinats_lng'); ?>">
                    </div>
                    <div class="elem for_save_btn_padding">
                        <input type="submit" value="Зберегти" class="btn btn-default width_auto">
                    </div>
                </form>
            </div>
        </div>

        <div class="one_settings_block">
            <div class="settings_block_title">
                Налаштування соціальних мереж
            </div>
            <div class="settings_block smaller">
                <form method="post" class="contact-form">
                    <div class="elem">
                        <label for="">Twitter</label>
                        <input type="text" name="social_twitter" value="<?= get_option('social_twitter'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">Facebook</label>
                        <input type="text" name="social_facebook" value="<?= get_option('social_facebook'); ?>">
                    </div>
                    <div class="elem">
                        <label for="">LinkedIn</label>
                        <input type="text" name="social_linkedin" value="<?= get_option('social_linkedin'); ?>">
                    </div>
                    <div class="elem for_save_btn_padding">
                        <input type="submit" value="Зберегти" class="btn btn-default width_auto">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>