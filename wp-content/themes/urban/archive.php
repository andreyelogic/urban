<?php
get_header();
$archive_posts = new WP_Query([
    'posts_per_page' => 9,
    'category__in' => [2, 4, 5],
    'paged' => get_query_var('paged')
]);
?>
    <div class="col-xs-12 archive_posts_wrapper">
        <div class="title_blog">
            <?php the_archive_title() ?>
        </div>
        <?php
        $columns = 4;
        if ($archive_posts->have_posts()) :
            ?>

            <div class="col-md-4 col-md-push-8 archive_navigation_wrapper">
                <?php wp_get_archives([
                    'type' => 'monthly',
                    'format' => 'html',
                    'echo' => 1,
                    'post_type' => 'post',
                ]); ?>
            </div>

            <div class="col-md-8 col-md-pull-4">
                <?php while ($archive_posts->have_posts()) :
                    $archive_posts->the_post(); ?>
                    <div class="col-md-<?= $columns; ?>">
                        <a href="<?= get_permalink(); ?>">
                            <div class="blog_image_post"
                                 style="background-image: url('<?= get_the_post_thumbnail_url() ?>')"></div>
                        </a>

                        <a href="<?= get_permalink(); ?>">
                            <div class="blog_title_post">
                                <?php the_title(); ?>
                            </div>
                        </a>
                    </div>
                <?php
                endwhile;
                ?>
                <?php
                if ($archive_posts->max_num_pages > 1) { ?>
                    <div class="posts_pagination col-md-12">
                        <?php
                        $page = get_query_var('paged');
                        if ($page == 0) {
                            $page = 1;
                        }
                        $i = 1;
                        while ($i < $archive_posts->max_num_pages + 1) { ?>
                            <a href="?paged=<?= $i; ?>" class="pag <?php if ($page == $i) {
                                echo 'active';
                            } ?> "><?= $i; ?></a>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                <?php }
                ?>
            </div>

        <?php
        else :
            get_template_part('template-parts/content', 'none');
        endif;
        ?>
    </div>
<?php
get_footer();
