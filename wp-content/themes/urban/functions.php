<?php
/**
 * urban functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package urban
 */
include 'inc/admin/advanced-options.php';

if (!function_exists('urban_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function urban_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on urban, use a find and replace
         * to change 'urban' to the name of your theme in all the template files.
         */
        load_theme_textdomain('urban', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'urban'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('urban_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'urban_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function urban_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('urban_content_width', 640);
}

add_action('after_setup_theme', 'urban_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function urban_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'urban'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'urban'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'urban_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function urban_scripts()
{
    wp_enqueue_style('urban-style', get_stylesheet_uri());
    wp_enqueue_style('urban-style-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('urban-style-main', get_template_directory_uri() . '/css/main.css');


    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
    wp_enqueue_script('urban-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);
    wp_enqueue_script('urban-main-script', get_template_directory_uri() . '/js/main.js');

    wp_enqueue_script('urban-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'urban_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}
function words_limit($input_text, $limit = 90, $end_str = '', $offset = null)
{
    $input_text = strip_tags($input_text);
    $words = explode(' ', $input_text); // создаём из строки массив слов
    if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
        return $input_text;
    }
    if ($offset !== null) {
        $words = array_slice($words, $offset); // укорачиваем массив до нужной длины
    }
    $words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины

    $out = implode(' ', $words);
    return $out . $end_str; //возвращаем строку + символ/строка завершения
}

function check_user_can_edit_language()
{
    $user = wp_get_current_user();
    if (in_array('editor', (array)$user->roles)) {
        $language = get_user_meta($user->ID, 'language_mod', true);
        wp_localize_script('urban-admin-script', 'CheckLanguage', ['language' => $language]);
        if ($_COOKIE['qtrans_admin_language'] != $language) {
            setcookie('qtrans_admin_language', $language);
            wp_redirect($_SERVER['REQUEST_URI']);
        }
    }
}

add_action('admin_menu', 'check_user_can_edit_language');

add_filter('get_the_date', 'func_to_to', 10, 3);
function func_to_to($the_date, $d, $post)
{

    $the_date = mysql2date($d, $post->post_date);
    return $the_date;
}

add_action('current_screen', 'checkProfile');
function checkProfile($current_screen)
{
    $user = wp_get_current_user();
    $role = ( array ) $user->roles;
    foreach ($role as $r)
    {
        if ( $r == 'administrator' )
        {
            if ($current_screen->base == 'user-edit') {
                ?>
                <style>
                    #acf-language_mod {
                        display: block !important;
                    }
                </style>
            <?php }
        }
    }

}

function get_right_content()
{
    $current_language = qtrans_getLanguage();
    wp_cache_delete(get_the_ID(), 'posts');
    $post = WP_Post::get_instance(get_the_ID());
    $contentCurrnetLang = qtranxf_use_language($current_language, $post->post_content, false, true);
    if ( $current_language == 'en' AND empty($contentCurrnetLang) )
    {
        if(!empty(qtranxf_use_language('fr', $post->post_content, false, true)))
        {
            $content = qtranxf_use_language('fr', $post->post_content, false, true);
        } elseif ( !empty(qtranxf_use_language('ua', $post->post_content, false, true)) )
        {
            $content = qtranxf_use_language('ua', $post->post_content, false, true);
        } elseif ( !empty(qtranxf_use_language('de', $post->post_content, false, true)) )
        {
            $content = qtranxf_use_language('de', $post->post_content, false, true);
        }
    } else {
        $content = $contentCurrnetLang;
    }
    return $content;
}

function checkTitleLanguage($title)
{

    $current_language = qtrans_getLanguage();
    wp_cache_delete(get_the_ID(), 'posts');
    $post = WP_Post::get_instance(get_the_ID());
    $titleCurrnetLang = qtranxf_use_language($current_language, $post->post_title, false, true);
    if ( $current_language == 'en' AND empty($titleCurrnetLang) )
    {
        if(!empty(qtranxf_use_language('fr', $post->post_title, false, true)))
        {
            $title = qtranxf_use_language('fr', $post->post_title, false, true);
        } elseif ( !empty(qtranxf_use_language('ua', $post->post_title, false, true)) )
        {
            $title = qtranxf_use_language('ua', $post->post_title, false, true);
        } elseif ( !empty(qtranxf_use_language('de', $post->post_title, false, true)) )
        {
            $title = qtranxf_use_language('de', $post->post_title, false, true);
        }
    } else {
        $title = $titleCurrnetLang;
    }

    return $title;
}
add_filter('the_title', 'checkTitleLanguage');



