<?php $pages = get_pages(['include' => '16,18']); ?>
</div><!-- .main_content_wrapper -->
</div><!-- #content -->
<footer class="container-fluid no_padding">
    <div class="container-fluid no_padding light_blue_background">
        <div class="container">
            <div class="col-xs-12 footer_wrapp">
                <div class="col-xs-4 footer-logo">
                    <img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt="">
                </div>
                <div class="col-xs-4 footer-links">
                    <a href="<?php echo get_permalink(16); ?>"> <?= $pages[0]->post_title; ?></a>
                    <a href="<?php echo get_permalink(18); ?>"> <?= $pages[1]->post_title; ?></a>

                </div>
                <div class="col-xs-4 footer-address">
                    <?= get_option('adress_street'); ?>,<br>
                    <?= get_option('adress_city'); ?>,<br>
                    <?= get_option('adress_country'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid no_padding dark_blue_background copyright">
        Chernivtsi Center for Border Town Research
    </div>
</footer>

<div class="container-fluid footer_mobile light_blue_background footer_mobile_margin">

    <div class="top_footer_mob_links">
        <a href="<?php echo get_permalink(16); ?>"> <?= $pages[0]->post_title; ?></a>
        <a href="<?php echo get_permalink(18); ?>"> <?= $pages[1]->post_title; ?></a>
    </div>

    <div class="footer-address">
        <?= get_option('adress_street'); ?>,<br>
        <?= get_option('adress_city'); ?>,<br>
        <?= get_option('adress_country'); ?>
    </div>

    <div class="no_padding social_links">
        <a href="<?= get_option('social_twitter'); ?>">
            <i class="fab fa-twitter"></i>
        </a>
        <a href="<?= get_option('social_facebook'); ?>">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a href="<?= get_option('social_linkedin'); ?>">
            <i class="fab fa-linkedin-in"></i>
        </a>
    </div>

    <div class="footer_mob_mail">
        <a href="mailto:<?= get_option('mail_for_contact'); ?>"><?= get_option('mail_for_contact'); ?></a>
    </div>


</div>

<div class="container-fluid footer_mobile no_padding dark_blue_background copyright">
    Copyright © 2018 Chernivtsi Centre for Cross Border Research
</div>

</div><!-- #page -->
<?php wp_footer();?>
</body>
</html>
