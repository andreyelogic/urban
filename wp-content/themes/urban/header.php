<?php $pages = get_pages(['include' => '65,68,70,3,11,13,8']); ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57055714-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-57055714-2');
	</script>


    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:600" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
    <header>
        <div class="container-fluid no_padding light_blue_background">
            <div class="container">
                <div class="col-xs-12 no_padding">
                    <div class="col-xs-3 no_padding language_switcher blue_background">
                        <?php qtrans_generateLanguageSelectCode('text', 'language'); ?>
                    </div>
                    <div class="col-xs-6 no_padding">
                        <div class="header_menu_top">
                            <a href="<?php echo get_permalink(65); ?>">
                                <?= $pages[0]->post_title ?>
                            </a>
                            <a href="<?php echo get_permalink(68); ?>"> <?= $pages[3]->post_title; ?></a>
                            <a href="<?php echo get_permalink(70); ?>"> <?= $pages[1]->post_title; ?></a>
                        </div>
                    </div>
                    <div class="col-xs-3 no_padding blue_background social_links">
                        <a href="<?= get_option('social_twitter'); ?>">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="<?= get_option('social_facebook'); ?>">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="<?= get_option('social_linkedin'); ?>">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-xs-12 border_left_right">
                <div class="col-xs-8 logo_wrap">
                    <a href="/"><img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
                    <a href="/">
                        <div class="logo_text">
                            Center for Border Town Research
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 contacts_top_wrap">
                    <div class="tel">
                        <i class="fas fa-phone"></i>
                        <a href=""><?= get_option('tel_num_one'); ?></a>
                        <a href=""><?= get_option('tel_num_two'); ?></a>
                    </div>
                    <div class="mail">
                        <img src="<?= get_template_directory_uri(); ?>/img/mail.png" alt="">
                        <a href="mailto:<?= get_option('mail_for_contact'); ?>"><?= get_option('mail_for_contact'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid no_padding light_blue_background">
            <div class="container">
                <div class="col-xs-12 no_padding main_menu_wrap">
                    <a href="<?php echo get_permalink(3); ?>"> <?= $pages[6]->post_title; ?></a>

                    <a href="<?php echo get_permalink(8); ?>">
                        <?= $pages[5]->post_title; ?>
                    </a>
                    <a href="<?php echo get_permalink(11); ?>">
                        <?= $pages[4]->post_title; ?>
                    </a>
                    <a href="<?php echo get_permalink(13); ?>">
                        <?= $pages[2]->post_title ?>
                    </a>
                </div>
            </div>
        </div>
    </header>

    <div class="header_mobile container-fluid light_blue_background no_padding">

        <div class="mob_header_top">
            <a href="<?php echo get_permalink(65); ?>"> <?= $pages[1]->post_title; ?></a>
            <a href="<?php echo get_permalink(68); ?>"> <?= $pages[3]->post_title; ?></a>
            <a href="<?php echo get_permalink(70); ?>"> <?= $pages[2]->post_title; ?></a>
            <div class="inline_wrap">
                <div id="nav-icon3">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>

        <div class="mob_menu_content">
            <a href="<?php echo get_permalink(3); ?>">
                <?= $pages[6]->post_title; ?>
            </a>
            <a href="<?php echo get_permalink(8); ?>">
                <?= $pages[5]->post_title; ?>
            </a>
            <a href="<?php echo get_permalink(11); ?>">
                <?= $pages[4]->post_title; ?>
            </a>
            <a href="<?php echo get_permalink(13); ?>">
                <?= $pages[0]->post_title; ?>
            </a>

            <div class="mob_lang_switcher">
                <?php qtrans_generateLanguageSelectCode('text', 'language'); ?>
            </div>

        </div>

    </div>

    <div class="header_mobile">

        <div class="mob_logo">
            <a href="/"><img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
        </div>

        <div class="logo_wrap">
            <div class="logo_text">
                Center for Border Town Research
            </div>
        </div>

        <div class="contacts_top_wrap">
            <div class="tel">
                <i class="fas fa-phone"></i>
                <a href=""><?= get_option('tel_num_one'); ?></a>
                <a href=""><?= get_option('tel_num_two'); ?></a>
            </div>
            <div class="mail">
                <a href="mailto:<?= get_option('mail_for_contact'); ?>">
                    <img src="<?= get_template_directory_uri(); ?>/img/mail.png" alt="">
                    <?= get_option('mail_for_contact'); ?>
                </a>
            </div>
        </div>

    </div>

    <div id="content" class="site-content container">
        <div class="main_content_wrapper border_left_right col-xs-12 no_padding">
