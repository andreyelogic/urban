<div class="col-xs-12 gallery_wrapp">
    <?php the_post();
    include get_template_directory() .'/template-parts/title-of-page.php';
    echo apply_filters('the_content', get_right_content()); ?>
</div>