<?php ?>
    <div class="title_blog">
        <?php
        the_title();
        ?>
    </div>
    <div class="col-xs-12">

<!--        --><?php
//        if (has_post_video(get_the_ID())) { ?>
<!--            <div class="col-xs-12 video_wrapper">-->
<!--                --><?php
//                echo do_shortcode('[featured-video-plus]'); ?>
<!--            </div>-->
<!--            --><?php
//        }
//        ?>

        <div class="single-content">
            <?php
            echo apply_filters('the_content', get_right_content());
            ?>
        </div>
    </div>

<?php




