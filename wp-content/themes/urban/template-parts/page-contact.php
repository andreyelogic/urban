<div class="col-xs-12">
    <div class="page_contakt_tit">
        <?= __($post->post_title); ?>
    </div>
    <div id="map_canvas" style="height: 325px;"></div>
    <div class="col-xs-12 contakts_info_wrap">
        <div class="contakt_margin_left_right">
            <div class="image"><i class="fas fa-map-marker-alt"></i></div>
            <div class="inf">
                <?= get_option('adress_street'); ?>,<br>
                <?= get_option('adress_city'); ?>,<br>
                <?= get_option('adress_country'); ?>
            </div>
        </div>
        <div class="contakt_margin_left_right">
            <div class="image"><i class="fas fa-phone"></i></div>
            <div class="inf">
                <div><?= get_option('tel_num_one'); ?></div>
                <div><?= get_option('tel_num_two'); ?></div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8kH3Xfo5pLqJpCMtDDGjszeIP4gms4ws"></script>
    <script>
        $ = jQuery;
        function initialize() {
            var myLatlng = new google.maps.LatLng(<?= get_option('coordinats_lat'); ?>, <?= get_option('coordinats_lng'); ?>);
            var myOptions = {
                center: myLatlng,
                zoom: 15,
                scrollwheel: false,
                disableDefaultUI: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: "Спа салон Імператриця"
            });
        }
        initialize();
    </script>

</div>