<div class="col-xs-12">
    <?php the_post(); ?>
    <?php include get_template_directory() .'/template-parts/title-of-page.php'; ?>
    <div class="col-xs-12 content_privacy_policy">
        <?php echo apply_filters('the_content', get_right_content()); ?>
    </div>
</div>