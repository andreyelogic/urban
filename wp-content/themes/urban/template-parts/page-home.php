<?php
$query = new WP_Query(array('cat' => '2, 4, 5, 6', 'posts_per_page' => 4));
?>
<div class="slider_home"><?= do_shortcode('[acf_gallery_slider acf_field="slider"]'); ?></div>
<div class="post_home">
    <?php
    while ($query->have_posts()) {
        $query->the_post(); ?>

            <div class="col-md-6">
                <div class="one_new_wrap col-xs-12">
                    <div class="col-md-4 no_padding">
                        <a href="<?= get_permalink(); ?>" class="one_new_read_more_btn">
                            <?php
                            if (has_post_video(get_the_ID())) { ?>
                                <div class="home_image_post">
                                    <?php
                                    echo do_shortcode('[featured-video-plus]'); ?>
                                </div>
                                <?php
                            } else { ?>
                                <div class="home_image_post"
                                     style="background-image: url('<?= get_the_post_thumbnail_url() ?>')"></div>
                            <?php }
                            ?>
                        </a>


                    </div>
                    <div class="col-md-8 no_padding left_padding_one_new">
                        <div class="one_new_date">
                            <?= get_the_date('d/m/Y'); ?>
                        </div>
                        <div class="one_new_title">
                            <a href="<?= get_permalink(); ?>" class="one_new_read_more_btn">
                                <?php echo words_limit(get_the_title(), '5', '...'); ?>
                            </a>
                        </div>
                        <div class="one_new_content">
                            <?php echo words_limit(apply_filters('the_content', get_right_content()), '4', '...'); ?>
                        </div>
                    </div>
                </div>
            </div>



        <?php
    } ?>
</div>
