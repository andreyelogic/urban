<?php
$cat_name = 'partners';
$per_page = 5;
$page = (get_query_var('page')) ? get_query_var('page') : 1;
$args = [ 'paged' => $page ];
if ( isset($cat_name) )
{
    $args['category_name'] = $cat_name;
}
if ( isset($per_page) )
{
    $args['posts_per_page'] = $per_page;
}
$query = new WP_Query($args); ?>

<div class="col-xs-12 partners_wrapper">

    <?php include get_template_directory() .'/template-parts/title-of-page.php'; ?>
    <?php
    if ( $query->have_posts() )
    {
        while ($query->have_posts()) {
            $query->the_post(); ?>
            <div class="col-md-12 one_partner no_padding">
                <?php
                if ( qtrans_getLanguage() == 'ua' AND get_field('partners_ukr_image', get_the_ID()) )
                { ?>

                    <a href="<?= get_the_content(); ?>" class="blog_image_post col-xs-2" style="background-image: url('<?= get_field('partners_ukr_image', get_the_ID()) ?>')"></a>

                <?php } else { ?>
                    <a href="<?= get_the_content(); ?>" class="blog_image_post col-xs-2" style="background-image: url('<?= get_the_post_thumbnail_url() ?>')"></a>
                <?php }
                ?>
                <div class="col-xs-10">
                    <a href="<?= get_the_content(); ?>">
                        <div class="blog_title_post"><?php the_title(); ?></div>
                    </a>
                </div>
            </div>
            <?php
        }
    } else {
        get_template_part('template-parts/content', 'none');
    } ?>


</div>
<?php
    if ($query->max_num_pages > 1) { ?>
    <div class="posts_pagination col-xs-12">
        <?php
        $i = 1;
        while ($i < $query->max_num_pages + 1) { ?>
            <a href="?page=<?= $i; ?>" class="pag <?php if ($page == $i) {
                echo 'active';
            } ?> " ><?= $i; ?></a>
            <?php
            $i++;
        }
        ?>
    </div>
<?php }
?>
