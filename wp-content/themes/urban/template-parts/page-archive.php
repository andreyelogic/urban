<?php

?>
<div class="col-xs-12">

    <div class="col-xs-12">
        <?php include get_template_directory() .'/template-parts/title-of-page.php'; ?>
    </div>

    <div class="col-md-4 col-md-push-8  archive_navigation_wrapper">
        <?php wp_get_archives([
            'type'            => 'monthly',
            'format'          => 'html',
            'echo'            => 1,
            'post_type'       => 'post',
        ]); ?>
    </div>

    <div class="col-md-8 col-md-pull-4">
        <?php
            $per_page = 9;
            $columns = 4;
            $category_in = [2, 4, 5];
            include get_template_directory() .'/inc/posts_pagination.php';
        ?>
    </div>



</div>
